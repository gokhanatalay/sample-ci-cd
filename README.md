# Sample CI CD



## Requirements
 - Docker
 - Nginx
 - Mysql

## Installation

### UBUNTU
#### Gitlab Runner
**Step #1: Add the Official GitLab Repository**
```bash
$ curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```
**Step #2: Install GitLab Runner on Ubuntu**
```bash
$ sudo apt-get install gitlab-runner
```

- Command to check GitLab Runner version
```bash
$ sudo gitlab-runner -version
```

*Output*
```bash
Version:      14.8.2
Git revision: c6e7e194
Git branch:   14-8-stable
GO version:   go1.17.7
Built:        2022-03-01T17:18:25+0000
OS/Arch:      linux/amd64
```

- To check status if GitLab Runner service is running or not
```bash
$ sudo gitlab-runner status
```
*Output*
```bash
Runtime platform arch=amd64 os=linux pid=4329 revision=c6e7e194 version=14.8.2
gitlab-runner: Service is running
```
**Step #3: Register GitLab Runner to GitLab on Ubuntu**

1. First login to GitLab Server with Username and Password.
2. Click on your project and select Settings
3. Navigate to Settings and click on CI/CD inside this click on Expand of Runners section
4. Copy GitLab server URL and Registration Token as shown below.
5. Paste GitLab Server URL and Token in registration command as below

```bash
$ gitlab-runner register --name sample-runner --url https://gitlab.com --registration-token YOUR_TOKEN
```
![gitlab-runner-install.gif](./docs/images/gitlab-runner-install.gif)

6. Introducing docker to runner 
```bash
$ nano /etc/gitlab-runner/config.toml
```
*Output*
```bash
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "sample-runner"
  url = "https://gitlab.com"
  token = "rezE8M7dcYC2g8fSozbB"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```
- We add [+"/var/run/docker.sock:/var/run/docker.sock"+] to the volumes array. 

*New File*
```bash
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "sample-runner"
  url = "https://gitlab.com"
  token = "rezE8M7dcYC2g8fSozbB"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "ruby:2.7"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    #Changed Line
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```
- Exit by ctrl+x -> y -> enter 

7. Check gitlab runner in Gitlab project -> Settings -> CI/CD -> Runners

![runner-check.png](./docs/images/runner-check.png)
